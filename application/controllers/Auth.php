<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {


    public function __construct() { 
        parent::__construct(); 
        $this->load->model('auth_model');
        $this->load->model('info_model');
    } 
    
	public function login()
	{
        //if user login
        if($this->session->has_userdata('logged_in')){
			redirect('/');
        }
        
        $data['title'] = 'Login';
        $this->load->view('layouts/auth/header',$data);
        $this->load->view('auth/login');
        $this->load->view('layouts/auth/footer');
    }
    
    public function daftar()
    {
        //if user login
        if($this->session->has_userdata('logged_in')){
			redirect('/');
        }
        
        $data['title'] = 'Daftar';
        $this->load->view('layouts/auth/header',$data);
        $this->load->view('auth/daftar');
        $this->load->view('layouts/auth/footer');

    }

    public function authentikasi(){
        $data['err'] = "";

        if( $this->input->post('submit') == 'daftar'){
            if($this->auth_model->daftar())
            {
                return redirect('/login');
            }
            else
            {
                $data['err'] = 'not valid';
                $data['title'] = 'data tidak valid';
                $this->load->view('layouts/auth/header',$data);
                $this->load->view('auth/daftar',$data);
                $this->load->view('layouts/auth/footer');
            }
        }
        elseif( $this->input->post('submit') == 'login'){
          $login = $this->auth_model->login();
          if(is_array($login)){
               redirect('/');
          }else{
                $data['err'] = $login;
                $data['title'] = 'login'." | ". $data['err'];
                
                $this->load->view('layouts/auth/header',$data);
                $this->load->view('auth/login',$data);
                $this->load->view('layouts/auth/footer');
          }
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }

}
