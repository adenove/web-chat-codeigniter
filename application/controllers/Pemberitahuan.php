<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemberitahuan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('logged_in')){
			redirect('/login');
		}
		$this->load->model('template_model');
		$this->load->model('pemberitahuan_model');
	}

	public function index()
	{	
		$data['unrd'] = $this->pemberitahuan_model->unreadpemberitahuan();
		$data['notif'] = $this->pemberitahuan_model->getpemberitahuan();
		$data['request'] = $this->pemberitahuan_model->friendneedacc();
		$this->template_model->template('page/pemberitahuan',$data);
	}

	public function pemberitahuanread()
	{
		$this->pemberitahuan_model->sendtoreadpemberitahuan();
	}
	
}
