<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('logged_in')){
			redirect('/login');
        }
		$this->load->model('info_model');
		$this->load->model('template_model');

	}

	public function index()
	{
	   $data['user']   = $this->info_model->info_user();
	   $data['profil'] = $this->info_model->have_profil();
       $data['status'] = $this->info_model->status();
	   $this->template_model->template('page/pengaturan',$data);
	}

	public function update(){
	   $this->info_model->updateuser();
	}

	
}
