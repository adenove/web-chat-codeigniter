<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teman extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('logged_in')){
			redirect('/login');
		}
		$this->load->model('teman_model');
		$this->load->model('pemberitahuan_model');
		$this->load->model('template_model');

	}

	public function index()
	{

		$data['teman'] = $this->teman_model->teman_user();
		$this->template_model->template('page/teman',$data);
	}

	public function temanmsg($teman){
		$this->teman_model->temanchek($teman);
		$data['nama'] = $teman;
		$this->template_model->template('page/message',$data);
	}

	public function request($id){
		$this->teman_model->requestfreind($id);
	}

	public function terimaperteman($uid,$id){
		$this->pemberitahuan_model->kirimpemberitahuan($uid,1);
		$this->teman_model->terimateman($id);
	}

	public function declinepertemanan($id){
		$this->teman_model->declinerequestfriend($id);
	}


}
