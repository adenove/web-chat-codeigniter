<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('logged_in')){
			redirect('/login');
        }
        
        $this->load->model('search_model');
        $this->load->model('template_model');
	}


	public function index()
	{
        $data['friends'] = $this->search_model->searchfriend();
        $this->template_model->template('page/search',$data);
	}


}
