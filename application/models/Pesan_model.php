<?php 

class Pesan_model extends CI_Model {


    public function __construct(){
        $this->load->database();
        $this->load->model('pesan_model');
        $this->load->model('info_model');
    }

    //kirim pesan
    public function kirimpesan($to){
        $pesan = array( 'from_user' => $this->session->name,
                        'to_user'   => $to,
                        'body'      => $this->input->post('pesan'),
                        'status'    => 'UNREAD'
                        );

        $this->db->insert('pesan',$pesan);
        

        return redirect('/pesan');
        
    }


    public function getpesan()
    {
        $this->db->select('*');
        $this->db->from('pesan');
        $this->db->group_start();
        $this->db->where('from_user',$this->session->name);
        $this->db->or_where('to_user',$this->session->name);
        $this->db->group_end();
        $this->db->order_by('pesan.waktu','DSC');
        $data = $this->db->get()->result();

        $chat = array();
        foreach($data as $dt){
            if($this->session->name == $dt->from_user){
                $mymsg = array('name' => $dt->to_user,'body'=>array(),'unread'=>0 ,'tanggal'=>'0','image_url' => $this->info_model->have_profil($dt->to_user));
            }

            elseif($this->session->name == $dt->to_user){
                $mymsg = array('name' => $dt->from_user,'body'=>array(),'unread'=>0,'tanggal'=>'0','image_url' => $this->info_model->have_profil($dt->from_user));
            }

            if(!in_array($mymsg,$chat)){
                array_push($chat,$mymsg);
            }

        }

        //filter array 
        foreach($chat as $cht => $val){
            foreach($data as $dts){
                 if($chat[$cht]['name'] == $dts->from_user or $chat[$cht]['name'] == $dts->to_user){
                    $mybdy = array( 'from_name' => $dts->from_user,
                                    'body'      => $dts->body,
                                    'status'    => $dts->status,
                                    'tanggal'   => $dts->waktu
                                 );
                    $chat[$cht]['body'][] = $mybdy;
                 }
            }
        }



        //count unread message
        foreach($chat as $cht => $val){
            foreach($chat[$cht]['body'] as $ky => $msgs ){
                if($msgs['from_name'] != $this->session->name && $msgs['status'] == "UNREAD"){
                    $chat[$cht]['unread'] += 1; 
                }
            }
        }
    

        foreach($chat as $k => $v){
            if($chat[$k]['name'] == $v['name']){
                $chat[$k]['tanggal'] = $v['body'][count($v['body'])-1]['tanggal'];
            }
            
        }

        // Sorting array by time (DESC ORDER)
        usort($chat, function($firstItem, $secondItem) {
            $timeStamp1 = strtotime($firstItem['tanggal']);
            $timeStamp2 = strtotime($secondItem['tanggal']);
            return $timeStamp2 - $timeStamp1;
        });

        return $chat;

    }


    public function have_unreadmsg(){
        $mymsg = $this->pesan_model->getpesan();
        $unread = 0;
        foreach($mymsg as $msg)
        {
           $unread += $msg['unread'];
        }
        return $unread;
    }

    public function message_isread(){
        
        $to_user = $this->input->post('to_user');
        $upd = array('status'=>'READ');
        $this->db->where('pesan.from_user',$to_user);
        $this->db->where('pesan.to_user',$this->session->name);
        $this->db->update('pesan',$upd);
        echo 'berhasil';

    }

}