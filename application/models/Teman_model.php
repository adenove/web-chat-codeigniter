<?php 

class Teman_model extends CI_Model {


    public function __construct(){
        $this->load->database();
        $this->load->model('info_model');
    }

    public function teman_user(){
        $user_name = $this->session->name;
        $u_id = $this->session->id;
        $this->db->select(array('friend_request.action','user.name','status.status'));
        $this->db->from('friend_request');
        $this->db->group_start();
        $this->db->where('friend_request.user_one_id',$u_id);
        $this->db->where('friend_request.action',2);
        $this->db->or_where('friend_request.user_two_id',$u_id);
        $this->db->group_end();
        $this->db->join('user','friend_request.user_two_id = user.id or friend_request.user_one_id = user.id');
        $this->db->join('user_status','user_status.username = user.name');
        $this->db->join('status','user_status.id_status = status.id');
        $this->db->where_not_in('user.name',$user_name);
        $friends = $this->db->get();
        return $friends->result();

    }

    public function requestfreind($id){
        $myid = $this->session->id;
        $idfriend = $id;

        $myarr = array(
                        'user_one_id'   => $myid,
                        'user_two_id'   => $idfriend,
                        'status'        => 0,
                        'action'        => 1
                      );

        $this->db->insert('friend_request',$myarr);
        
        return redirect('/');
    }


    public function terimateman($id){

        $upd = array('action' => 2);
        $this->db->where('friend_request.id',$id);
        $this->db->update('friend_request',$upd);
        
        return redirect('/teman');

    }


    public function declinerequestfriend($id){
        $this->db->delete('friend_request',array('id'=>$id));
        return redirect('/');
    }

    public function temanchek($name){
        $this->db->select('user.name');
        $this->db->from('user');
        $this->db->where('user.name',$name);
        $data = $this->db->get()->result();
        if(count($data) < 1 )
        {
            redirect('/404_override');
        }
    }


}