<?php 

class Search_model extends CI_Model {


    public function __construct(){
      $this->load->database();
      $this->load->model('info_model');
    }



    public function searchfriend(){
     //get user
     $query = $this->input->get('query');
     $user_name = $this->session->name;
     $id = $this->session->id;
     $this->db->select(array('user.id','user.full_name','name','id_status','status.status'));
     $this->db->from('user');
     $this->db->join('user_status','user.name = user_status.username');
     $this->db->join('status','user_status.id_status = status.id');
     $this->db->like('user.name',$query);
     $this->db->where_not_in('name', $user_name);
     $mydt = $this->db->get()->result();

    //get status friend or no friend
     $this->db->select(array('action','user_two_id','user_one_id','user.name'));
     $this->db->from('friend_request');
     $this->db->group_start();
     $this->db->where('friend_request.user_two_id',$id);
     $this->db->or_where('friend_request.user_one_id',$id);
     $this->db->group_end();
     $this->db->join('user','user.id = friend_request.user_one_id or user.id = friend_request.user_two_id');
     $this->db->where_not_in('user.id',$id);

     $actions = $this->db->get();
     
     $ui = array();
     $mysearch = array();
     if(count($mydt) > 0)
     {
       //add default action = 0;
        foreach($mydt as $dt => $val)
        {
            $dts = array( "id" => $val->id,"name" => $val->name,"full_name"=>$val->full_name,"image_url"=>$this->info_model->have_profil($val->name),"id_status" => $val->id_status,"status" => $val->status,'action' => 0);
            $mysearch[] = $dts;
            $ui[$val->name] = $dts;
        }
       //when user have action relasi set
        foreach($actions->result() as $action)
        {
          foreach($ui as $u => $vl)
          {
            if($action->name == $u)
            {
              $ui[$u]['action'] = $action->action;
              $ui[$u]['id_two'] = $action->user_two_id; 
            }
          }
        }
      }
    
    return $ui;

    }


}