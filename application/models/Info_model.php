<?php 

class Info_model extends CI_Model {


    public function __construct(){
        $this->load->database();
    }

    public function info_user(){
        $user_name = $this->session->name;
        $this->db->select(array('user.id','name','email','user_status.id_status','status.status','user.full_name'));
        $this->db->from('user');
        $this->db->where('name',$user_name);
        $this->db->join('user_status','user.name = user_status.username');
        $this->db->join('status','status.id = user_status.id_status');
        $info_user = $this->db->get();


        return $info_user->result();
    }


    public function have_profil($id = ""){
        if($id == ""){
            $id = $this->session->id;
        }else{
            $this->db->select('user.id');
            $this->db->from('user');
            $this->db->where('user.name',$id);
            $id = $this->db->get();
            $id = $id->result();
            $id = $id[0]->id;
        }
        
        $location_profil;
        $default = './upload/default_u/blank.png';
        $png_img = './upload/'.$id.'/profil.png';
        $jpg_img = './upload/'.$id.'/profil.jpg';
        $gif_img = './upload/'.$id.'/profil.gif';

        //user profil
        if(file_exists($png_img))
        {
            $location_profil = base_url().'upload/'.$id.'/profil.png';
        }
        elseif(file_exists($jpg_img))
        {
            $location_profil = base_url().'upload/'.$id.'/profil.jpg';
        }
        elseif(file_exists($gif_img))
        {
            $location_profil = base_url().'upload/'.$id.'/profil.gif';
        }
        else
        {
            $location_profil = base_url().'upload/default_u/blank.png';
        }

        return $location_profil;

    }

    public function status(){
        $this->db->select('*');
        $this->db->from('status');
        $status = $this->db->get();
        return $status->result();
    }


    public function updateuser(){
        $id = $this->session->id;
        //image
        if (!is_dir('upload/'.$id))
        {
            mkdir('./upload/' . $id, 0777, TRUE);
        }
        else 
        {
            if($_FILES['image_profil']['error'] == 0)
            { 
                //The name of the folder.
                $folder = './upload/' . $id;
                
                //Get a list of all of the file names in the folder.
                $files = glob($folder . '/*');
                
                //Loop through the file list.
                foreach($files as $file){
                    //Make sure that this is a file and not a directory.
                    if(is_file($file)){
                        //Use the unlink function to delete the file.
                        unlink($file);
                    }
                }
                
                $config['upload_path']          = './upload/'.$id;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['file_name']            = 'profil';
                $config['max_size']             = 1000;
                $config['overwrite']			= true;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image_profil')) {
                    $this->upload->data("file_name");
                }
            }
        
        }
        
        $namepanjang = $this->input->post('name_panjang');
        $upd_usr = array('full_name'=>$namepanjang);
        $this->db->where('user.id',$id);
        $this->db->update('user',$upd_usr);

        $email = $this->input->post('email');
        if($this->chekemail($email))
        {
            $upd_usr = array('email'=>$email);
            $this->db->where('user.id',$id);
            $this->db->update('user',$upd_usr);

        }


        $status = $this->input->post('status');
        $u_stts = array('id_status'=>$status);
        $this->db->where('user_status.username',$this->session->name);
        $this->db->update('user_status',$u_stts);

        return redirect('/pengaturan');

        
    }


    public function chekemail($email){
        $this->db->select('user.email');
        $this->db->from('user');
        $this->db->where('user.email',$email);
        $this->db->where_not_in('user.id',$this->session->id);
        $data = $this->db->get()->result();
        if(count($data) > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

}