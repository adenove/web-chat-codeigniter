<?php 

class Template_model extends CI_Model {


    public function __construct(){
        $this->load->model('pesan_model');
        $this->load->model('info_model');
        $this->load->model('pemberitahuan_model');
    }

    public function template($main_view,$data = array()){
        $data['pemb_unrd'] = $this->pemberitahuan_model->unreadpemberitahuanall();
        $data['sidebar'] = $this->pesan_model->have_unreadmsg();
        $data['mystatus'] = $this->info_model->info_user();
        $data['fotoprofil'] = $this->info_model->have_profil();
        $this->load->view('layouts/v1/header',$data);
        $this->load->view($main_view,$data);
        $this->load->view('layouts/v1/footer');
    }
    

}