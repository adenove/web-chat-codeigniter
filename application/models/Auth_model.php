<?php 

class Auth_model extends CI_Model {


    public function __construct(){
        $this->load->database();
    }


    public function login(){
        // $this->load->helper('url');
        $this->db->select('user.id,user.name,user_status.id_status,user.password');
        $this->db->from('user');
        $this->db->join('user_status','user.name = user_status.username');
        $this->db->join('status','status.id = user_status.id_status');
        $this->db->where('name',$this->input->post('user'));
        $this->db->or_where('email',$this->input->post('user'));
        $mydata = $this->db->get();
        if(!empty($mydata->result())){
            foreach($mydata->result() as $usr ){
                if($usr->password == $this->input->post('password')){
                    $mydata = array(
                                    'id'        => $usr->id,
                                    'name'      => $usr->name,
                                    'logged_in' => TRUE
                                    );

                    $this->session->set_userdata($mydata);
                    return $mydata;
                }else{
                    return $mydata = "kata sandi salah";
                }
            }
        }else {
            return $mydata = "Akun Belum Terdaftar";
        }
        
        foreach($mydata->result() as $usr ){
            if($usr->password == $this->input->post('user')){
                echo $this->input->post('user');
            }
        }

    }

    public function daftar(){
        $this->load->library('form_validation');
        $username = preg_replace('/\s+/', '', $this->input->post('username'));

        if($this->username_check($username,$this->input->post('email'))){
            $this->form_validation->set_rules('username', 'username', 'required|alpha_numeric|max_length[25]');
            $this->form_validation->set_rules('fullname', 'full_name', 'required|max_length[100]');
            $this->form_validation->set_rules('email', 'email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
            if ($this->form_validation->run() == FALSE)
            {
                return FALSE;
            }
            else
            {
                $data = array(
                    'name'     => $username,
                    'full_name'=> $this->input->post('fullname'),
                    'email'    => $this->input->post('email'),
                    'password' => $this->input->post('password')
                );
    
                $status = array(
                                'username'  => $this->input->post('username'),
                                'id_status' => 1
                );
    
                $this->db->insert('user',$data);
                $this->db->insert('user_status',$status);
    
                return TRUE;
            }
        }

        return redirect('/as/dada');

    
    }


    public function username_check($str,$eml){
        $this->db->select('user.name');
        $this->db->from('user');
        $this->db->where('user.name',$str);
        $this->db->or_where('user.email',$eml);
        $chek = $this->db->get()->result();
        if(count($chek) >= 1)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }

    }

}