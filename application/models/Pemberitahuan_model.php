<?php 

class Pemberitahuan_model extends CI_Model {


    public function __construct(){
      $this->load->database();
      $this->load->model('pemberitahuan_model');
    }

    public function friendneedacc(){
        $user_name = $this->session->name;
        $u_id = $this->session->id;
        $this->db->select(array('friend_request.id','friend_request.action','user.id as uid','user.name'));
        $this->db->from('friend_request');
        $this->db->where('friend_request.action',1);
        $this->db->where('friend_request.user_two_id',$u_id);
        $this->db->join('user','user.id = friend_request.user_one_id');
        $this->db->where_not_in('user.name',$user_name);
        $friend_r = $this->db->get()->result();
        
        return $friend_r;
    }

    public function kirimpemberitahuan($id,$msa){
      $insert = array( 'to_id'=>$id,'from_id'=>$this->session->id,'message_action'=>$msa,'status'=>'UNREAD');
      $this->db->insert('pemberitahuan',$insert);
      return 'berhasil';
    }

    public function getpemberitahuan(){
      $id = $this->session->id;
      $this->db->select(array('pemberitahuan.id','user.name','pemberi_info.info','pemberitahuan.status','pemberitahuan.waktu'));
      $this->db->from('pemberitahuan');
      $this->db->where('pemberitahuan.to_id',$id);
      $this->db->join('user','user.id = pemberitahuan.from_id');
      $this->db->join('pemberi_info','pemberi_info.id = pemberitahuan.message_action');
      $this->db->order_by('pemberitahuan.waktu','DESC');
      $pemberitahuan = $this->db->get();
      return $pemberitahuan->result();
    }


    public function unreadpemberitahuan(){
      $unread = 0;
      $getpem = $this->pemberitahuan_model->getpemberitahuan();
      if(count($getpem)>0)
      {
        foreach($getpem as $notif)
        {
          if($notif->status == 'UNREAD')
          {
            $unread += 1;
          }
        }
      }
      return $unread;
  }

    public function unreadpemberitahuanall(){
        $unread = 0 + count($this->pemberitahuan_model->friendneedacc());
        $getpem = $this->pemberitahuan_model->getpemberitahuan();
        if(count($getpem)>0)
        {
          foreach($getpem as $notif)
          {
            if($notif->status == 'UNREAD')
            {
              $unread += 1;
            }
          }
        }
        return $unread;
    }

    public function sendtoreadpemberitahuan(){
      $id = $this->input->post('id_p');
      $upd = array('status'=>'READ');
      $this->db->where('pemberitahuan.id',$id);
      $this->db->update('pemberitahuan',$upd);
    }


}