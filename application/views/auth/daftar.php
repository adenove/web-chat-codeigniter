	<div class="side">
		<img width="100" src="<?php echo base_url();?>/upload/chat-logo/chat.png" alt="chat">
		<span class="login100-form-title p-b-51">
			<br>
			<!-- <strong style="text-align:left;" >Insades</strong> <br> -->
			<small>Ngobrol disini yuk</small>
		</span>
	</div>  
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-50 p-b-90">
				<form class="login100-form validate-form flex-sb flex-w" method="post" action="<?php echo base_url('auth/authentikasi'); ?>" > 
					<span class="login100-form-title p-b-51">
						Daftar
					</span>
					<?php if(!empty($err)){ ;?>
                        <div class="alert alert-danger w-100 text-center" role="alert">
							ada kesalahan silahkan isi dengan benar
						</div>
                     <?php };?>
					<div class="wrap-input100 m-b-16" data-validate = "Username is required">
						<input class="input100" type="text" name="username" placeholder="Username" data-toggle="tooltip" data-placement="left" title="Symbol not Allowed & max 25 character">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 m-b-16" data-validate = "Full name is required">
						<input class="input100" type="text" name="fullname" placeholder="Nama Panjang"  data-toggle="tooltip" data-placement="left" title="Required">
						<span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 m-b-16" data-validate = "email is required">
						<input class="input100" type="email" name="email" placeholder="Email"  data-toggle="tooltip" data-placement="left" title="Required">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 m-b-16" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Password"  data-toggle="tooltip" data-placement="left" title="Min 6 character">
						<span class="focus-input100"></span>
					</div>
					<div class="flex-sb-m w-full p-t-3 p-b-24">
                    <p class="txt1" > Sudah Punya Akun? <a href="<?php echo base_url();?>login"><b>Login</b></a></p>
					</div>
					<div class="container-login100-form-btn m-t-17">
						<button class="login100-form-btn" type="submit" name="submit" value="daftar" >
							Daftar
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>