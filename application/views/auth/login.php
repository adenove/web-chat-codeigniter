 <div class="side">
	<img width="100" src="<?php echo base_url();?>/upload/chat-logo/chat.png" alt="">
	<span class="login100-form-title p-b-51">
		<br>
		<!-- <strong style="text-align:left;" >Insades</strong> <br> -->
		<small>Ngobrol disini yuk</small>
	</span>
 </div>
   <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-50 p-b-90">
                <form class="login100-form validate-form flex-sb flex-w" method="post" action="<?php echo base_url('auth/authentikasi'); ?>">
					<span class="login100-form-title p-b-51">
						Login
                    </span>
                    <?php if(!empty($err)){ ;?>
                        <div class="box login100-form-title">
                            <span class="login100-form-title p-b-51">
                                <?php echo $err ;?>
                            </span>
                        </div>
                     <?php };?>
					<div class="wrap-input100 m-b-16" data-validate = "Username is required">
						<input class="input100" type="text" name="user" placeholder="Username or Email">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 m-b-16" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
                    </div>
                    <br>
					<div class="flex-sb-m w-full p-t-3 p-b-24">
                        <p class="txt1" > Belum Punya Akun? <a href="<?php echo base_url();?>daftar"><b>Daftar</b></a></p>
					</div>
					<div class="container-login100-form-btn m-t-17">
						<button class="login100-form-btn" name="submit" value="login">
							Login
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>