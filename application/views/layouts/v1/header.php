<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/home/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <title>Insades</title>
  </head>
  <body style="position: relative !important;min-height: 100% !important;height:100%;" >
    <div class="admin d-flex" style="min-height:100%;">
        <!-- sidebar -->
        <div class="pl-5 pr-5 bg-dark text-white sidebar">
          <div class="profil">
              <form class="mt-5" action="<?php echo base_url('search') ;?>" methods="GET">
                <div class="input-s">
                  <input type="text" name="query" class="form-control search" placeholder="Cari Teman" autocomplete="off">
                  <button type="submit" class="btn input-group-text road"><i class="fa fa-search"></i></button>
                </div>
              </form>
            <img src="<?php echo $fotoprofil;?>?t=<?=time()?>" alt="profil" class="rounded-circle mx-auto d-block " width="150px" height="150px" >
            <p class="text-center pt-4"><?php echo $mystatus[0]->full_name;?></p>
            <p class="text-center text-muted">"<?php echo $mystatus[0]->status;?>"</p>
          </div>
          <hr>
          <div class="list-link pt-1">
            <ul class="list-group list-group-flush p-0">
              <li class="list-group-item bg-dark d-flex justify-content-between align-items-center"><a href="<?php echo base_url();?>pesan">Pesan</a> <span class="badge badge-primary badge-pill"><?php echo $sidebar;?></span></li>
              <li class="list-group-item bg-dark d-flex justify-content-between align-items-center"><a href="<?php echo base_url();?>pemberitahuan">Pemberitahuan</a> <span class="badge badge-primary badge-pill"><?php echo $pemb_unrd;?></span></li>
              <li class="list-group-item bg-dark d-flex justify-content-between align-items-center"><a href="<?php echo base_url();?>teman">Teman</a></li>
              <li class="list-group-item bg-dark d-flex justify-content-between align-items-center"><a href="<?php echo base_url();?>pengaturan">Pengaturan</a></li>
              <li class="list-group-item bg-dark d-flex justify-content-between align-items-center"><a href="<?php echo base_url();?>logout">Keluar</a></li>
            </ul>
          </div>
        </div>
        <div class="p-2 flex-grow-1">