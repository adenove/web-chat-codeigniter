          <div class="d-flex justify-content-center align-items-center h-100">
          <?php if(count($pesans) < 1 ){;?>
          <div class="group d-flex flex-column justify-content-center">
            <h1 class="text-muted" >Mulai Mengobrol</h1>
            <hr>
            <form class="mt-3" action="<?php echo base_url('search') ;?>" methods="GET">
              <div class="input-s">
                <input type="text" name="query" class="form-control form-control-lg inp-ngobrol" placeholder="Cari Teman" autocomplete="off">
                <button type="submit" class="btn input-group-text road inp-srch"><i class="fa fa-search"></i></button>
              </div>
            </form>
          </div>
          <?php } else { ?>
            <div class="row w-75 h-75">
              <div class="col-4 account">
                <div class="side-account">
                  <ul class="list-group list-group-flush">
                    <?php foreach($pesans as $pesan ){;?>
                    <li class="list-group-item" data-message="<?php echo $pesan['name'] ;?>">
                      <div class="d-flex justify-content-between align-items-center">
                        <img src="<?php echo $pesan['image_url'] ;?>?t=<?=time()?>" alt="avatar" width="50px" height="auto" class="rounded-circle m-1">
                        <div class="chat-group text-left mt-3 w-100 pl-3">
                          <p class="m-0 d-flex justify-content-between align-items-center" ><strong><?php echo $pesan['name'] ;?></strong><?php if($pesan['unread'] > 0){ echo "<span class='badge badge-pill badge-primary'>". $pesan['unread'] ."</span>"; };?></p>
                          <!-- <p class="text-muted" ><small><?php //echo substr($pesan['body'][count($pesan['body'])-1]['body'],0,15);?></small></p> -->
                          <p class="text-muted" >
                            <small>
                              <?php 
                              if( strlen($pesan['body'][count($pesan['body'])-1]['body']) > 15 ){ 
                                  echo substr($pesan['body'][count($pesan['body'])-1]['body'],0,15) . '...';
                                }else{
                                  echo substr($pesan['body'][count($pesan['body'])-1]['body'],0,15);
                                };
                              ?>
                            </small>
                          </p>
                        </div>
                      </div>
                    </li>
                    <?php };?>
                  </ul>
                </div>
              </div>
              <div class="col-8 messg position-relative">
                <?php foreach($pesans as $pesan){;?>
                  <div class="pesan <?php echo $pesan['name'] ;?>">
                    <?php foreach($pesan['body'] as $chat ){;?>
                      <div class="mesg <?php if($chat['from_name']==$_SESSION['name']){ echo "userisme"; };?>">
                        <div class="send-user">
                        <?php if($chat['from_name']!=$_SESSION['name']){;?>
                          <p><b><?php echo $chat['from_name'];?></b></p>
                        <?php }; ?>
                          <!-- <hr class="pt-1 pb-1 mt-2 mb-0" > -->
                          <p class="text-justify"><?php echo $chat['body'];?></p>
                        </div>
                      </div>  
                    <?php };?>
                </div>
                <?php };?>
                <form class="position-relative w-100 send" action="<?php echo base_url('send/'); ?>" method="POST">
                  <div class="input-s">
                    <textarea class="form-control border-0 pesan-kirim click-pesan" placeholder="Masukan pesan" name="pesan"></textarea>
                    <button type="submit" class="btn input-group-text road"><i class="fas fa-paper-plane"></i></button>
                  </div>
                </form>
              </div>
            </div>
            
          <?php };?>

          </div>