
        <div class="p-2 flex-grow-1">
          <div class="h-100">
            <div class="teman p-5">
              <div class="title">
                <div class="alert alert-dark" role="alert">
                  <h5 class="m-0" >Search : <?php echo $this->input->get('query');?></h5>
                </div>
                <hr>
              </div>
              <div class="__list">
                <div class="row">
                  <div class="col-12">
                    <div class="d-flex flex-column __l-t">
                     <?php if (count($friends)<= 0){;?>
                      <div class="alert alert-warning" role="alert">
                       <?php echo "<h6 class='m-0' >Nama pengguna Tidak Ditemukan </h6>";?>  
                      </div>
                     <?php };?>
                     <?php foreach($friends as $tmn ){?>
                      <div class="d-flex align-items-center justify-content-between" href="p/<?php echo $tmn['name'] ;?>">
                        <div class="d-flex justify-content-start align-items-center">
                          <img src="<?php echo $tmn['image_url'];?>?t=<?=time()?>" alt="avatar" width="50px" height="50px" class="rounded-circle m-1">
                          <div class="chat-group text-left mt-3 pl-3">
                            <p class="m-0 " ><strong> <?php echo $tmn['full_name'];?> <small class="text-muted" > | @<?php echo $tmn['name'];?></small> </strong></p>
                            <p class="text-muted" ><small><?php echo $tmn['status'];?></small></p>
                          </div>
                        </div>
                        <div class="add-fr">
                          <?php if($tmn['action'] == 0){?>
                            <a href="<?php echo base_url('request-friend');?>/<?php echo $tmn['id'];?>" class="btn btn-info text-white teman">Tambahkan Teman</a>
                          <?php } elseif($tmn['action'] == 1){?>
                            <?php if($tmn['id_two'] == $this->session->id ){;?>
                              <a href="<?php echo base_url('pemberitahuan');?>" class="btn btn-danger text-white teman">Konfirmasi Teman</a>
                            <?php } else {?>
                              <a href="#" class="btn btn-secondary text-white disabled" role="button" aria-disabled="true">Permintaan Pertemanan Terkirim</a>
                            <?php };?>
                          <?php } elseif($tmn['action'] == 2){?>
                            <a href="<?php echo base_url('p');?>/<?php echo $tmn['name'];?> " class="btn btn-info text-white">Kirim Pesan</a>
                          <?php } ;?>
                        </div>
                      </div>
                    <?php };?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>