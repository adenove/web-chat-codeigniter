
        <div class="p-2 flex-grow-1">
          <div class="h-100">
            <div class="teman p-5">
              <div class="title">
                <!-- <h3>Pemberitahuan</h3> -->
                <!-- <hr> -->
              </div>
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Pemberitahuan <span class="badge badge-info"><?php echo $unrd;?></span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Friend Request <span class="badge badge-info"><?php echo count($request);?></span></a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                  <div class="__list">
                    <div class="list-group list-group-flush pemberitahuan">
                      <?php foreach($notif as $notf){?>
                      <a data-notif="<?php echo $notf->id;?>" href="<?php echo base_url('teman');?>" class="link-noftif list-group-item list-group-item-action flex-column align-items-start <?php if($notf->status == 'UNREAD'){ echo "bg-success text-white"; };?>">
                        <div class="d-flex w-100 justify-content-between">
                          <h5 class="mb-1"><?php echo $notf->name;?></h5>
                          <small><?php echo $notf->waktu;?></small>
                        </div>
                        <p class="mb-1"><?php echo $notf->info;?></p>
                      </a>
                      <?php };?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                  <div class="__list">
                    <div class="list-group list-group-flush pemberitahuan">
                      <?php foreach($request as $req) {;?>
                      <div href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                          <h5 class="mb-1"><?php echo $req->name;?></h5>
                          <!-- <small>3 days ago</small> -->
                        </div>
                        <!-- <p class="mb-2 text-muted">Meminta Pertemanan</p> -->
                        <div class="d-flex align-items-end justify-content-between">
                          <p class="m-0 text-muted">Meminta Pertemanan</p>
                          <div class="d-flex align-items-center ">
                            <a class="btn btn-success mr-3" href="<?php echo base_url('terima');?>/<?php echo $req->uid;?>/<?php echo $req->id;?>" role="button">Terima</a>
                            <a class="btn btn-danger mr-3" href="<?php echo base_url('batalkan');?>/<?php echo $req->id;?>" role="button">Batalkan</a>
                          </div>
                        </div>
                      </div>
                      <?php };?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>