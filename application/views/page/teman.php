<!-- load model -->
<?php $CI =& get_instance();$CI->load->model('info_model'); ?>

        <div class="flex-grow-1">
          <div class="h-100">
            <div class="teman p-5">
              <div class="title">
                <h3>Teman</h3>
                <hr>
              </div>
              <div class="__list">
                <div class="row">
                  <div class="col-12">
                    <div class="d-flex flex-column __l-t">
                    <?php foreach($teman as $tmn ){?>
                     <?php if($tmn->action == 2){?>
                      <a href="p/<?php echo $tmn->name ;?>">
                        <div class="d-flex justify-content-start align-items-center tmn">
                          <img src="<?php echo $CI->info_model->have_profil($tmn->name);?>?t=<?=time()?>" alt="avatar" width="50px" height="50px" class="rounded-circle m-1">
                          <div class="chat-group text-left mt-3 pl-3">
                            <p class="m-0 " ><strong> <?php echo $tmn->name;?> </strong></p>
                            <p class="text-muted" ><small><?php echo $tmn->status;?></small></p>
                          </div>
                        </div>
                      </a>
                     <?php } ;?>
                    <?php };?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>