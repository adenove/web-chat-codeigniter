        <div class=" flex-grow-1">
          <div class="h-100">
            <div class="teman p-5">
              <div class="title">
                <h3>Pengaturan</h3>
                <hr>
              </div>
              <div class="__list">
                <div class="list-group list-group-flush pemberitahuan">
                  <div class="row ">
                    <div class="col-12 d-flex justify-content-center">
                      <div class="profil d-flex flex-column justify-content-center">
                        <img src="<?php echo $profil;?>?t=<?=time()?>" alt="profil" class="rounded-circle mx-auto d-block mt-4" width="150px" height="150px">
                        <div class="custom-file mt-4 d-flex flex-column justify-content-center">
                          <div class="form-group border">
                            <input form="info" type="file" class="form-control-file" name="image_profil">
                          </div>
                        </div>  
                        <br>
                      </div>
                    </div>
                    <div class="col-12 d-flex justify-content-center">
                      <form action="<?php echo base_url('updateinfo');?>" class="d-block mt-4 w-100 pr-4" method="POST" id="info" enctype="multipart/form-data">
                        <div class="form-group d-flex align-items-center">
                          <label class="m-0 p-0 col-2" for="namepanjang">Nama Panjang</label>
                          <input type="text" id="namepanjang" class="form-control mx-sm-3 col-10" value="<?php echo $user[0]->full_name;?>" name="name_panjang">
                        </div>
                        <br>
                        <div class="form-group d-flex align-items-center">
                          <label class="m-0 p-0 col-2" for="email">Email</label>
                          <input type="email" id="email" class="form-control mx-sm-3 col-10" value="<?php echo $user[0]->email;?>" name="email">
                        </div>
                        <br>
                        <div class="form-group d-flex align-items-center">
                          <label class="m-0 p-0 col-2" for="inlineFormCustomSelect">Status</label>
                          <select class="custom-select mx-sm-3 col-10" id="inlineFormCustomSelect" name="status">
                            <?php foreach($status as $sts){ ;?>
                              <option  <?php if( $user[0]->id_status == $sts->id ) {;?> selected <?php } ;?>  value="<?php echo $sts->id;?>" ><?php echo $sts->status;?></option>
                            <?php };?>
                          </select>
                        </div>
                        <br>
                        <div class="text-right">
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>