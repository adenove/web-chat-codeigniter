-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Oct 04, 2019 at 01:00 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insades`
--

-- --------------------------------------------------------

--
-- Table structure for table `friend_request`
--

CREATE TABLE `friend_request` (
  `id` int(225) NOT NULL,
  `user_one_id` varchar(100) NOT NULL,
  `user_two_id` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `action` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `friend_request`
--

INSERT INTO `friend_request` (`id`, `user_one_id`, `user_two_id`, `status`, `action`) VALUES
(89, '18', '17', '0', '2'),
(90, '17', '19', '0', '2');

-- --------------------------------------------------------

--
-- Table structure for table `pemberitahuan`
--

CREATE TABLE `pemberitahuan` (
  `id` int(225) NOT NULL,
  `to_id` int(100) NOT NULL,
  `from_id` int(100) NOT NULL,
  `message_action` int(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pemberitahuan`
--

INSERT INTO `pemberitahuan` (`id`, `to_id`, `from_id`, `message_action`, `status`, `waktu`) VALUES
(17, 18, 17, 1, 'READ', '2019-03-07 20:47:52'),
(18, 17, 19, 1, 'READ', '2019-10-04 07:37:10');

-- --------------------------------------------------------

--
-- Table structure for table `pemberi_info`
--

CREATE TABLE `pemberi_info` (
  `id` int(225) NOT NULL,
  `info` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pemberi_info`
--

INSERT INTO `pemberi_info` (`id`, `info`) VALUES
(1, 'menerima pertemanan anda');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id` int(225) NOT NULL,
  `from_user` varchar(100) NOT NULL,
  `to_user` varchar(100) NOT NULL,
  `body` varchar(250) NOT NULL,
  `status` varchar(20) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id`, `from_user`, `to_user`, `body`, `status`, `waktu`) VALUES
(119, 'adenovendra19', 'pakeko', 'x', 'READ', '2019-03-07 17:40:11'),
(120, 'adenovendra19', 'pakeko', 'z', 'READ', '2019-03-07 17:40:16'),
(121, 'pakeko', 'adenovendra19', 's', 'READ', '2019-03-07 17:41:49'),
(122, 'adenovendra19', 'pakeko', 'hahah', 'READ', '2019-03-07 17:42:06'),
(123, 'adenovendra19', 'pakeko', 'test', 'READ', '2019-03-07 17:42:12'),
(124, 'pakeko', 'adenovendra19', 'ui\r\n', 'READ', '2019-03-07 17:42:28'),
(125, 'pakeko', 'adenovendra19', 'tes', 'READ', '2019-03-07 17:42:33'),
(126, 'pakeko', 'adenovendra19', 'aa', 'READ', '2019-03-07 17:42:38'),
(127, 'emon', 'adenovendra19', 'hallo', 'READ', '2019-10-04 07:48:45'),
(128, 'adenovendra19', 'emon', 'oe', 'READ', '2019-10-04 07:51:18'),
(129, 'emon', 'adenovendra19', 'sss', 'READ', '2019-10-04 07:51:27'),
(130, 'adenovendra19', 'emon', 'oe', 'UNREAD', '2019-10-04 07:53:01');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(225) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status`) VALUES
(1, 'Ada'),
(2, 'Sibuk'),
(3, 'Sedang Pergi'),
(4, 'Sedang Berlibur');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(225) NOT NULL,
  `name` varchar(100) NOT NULL,
  `full_name` varchar(20) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `password` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `full_name`, `email`, `password`) VALUES
(17, 'oke', 'aku', 'oke@gmail.com', 'oke'),
(18, 'pakeko', 'eko emon', 'eko123@gmail.com', 'ako123'),
(19, 'emon', 'pak emon', 'emon@gmail.com', 'emon123'),
(20, 'pakemon', 'eko emon', 'as@gmail.com', 'adenovendra');

-- --------------------------------------------------------

--
-- Table structure for table `user_status`
--

CREATE TABLE `user_status` (
  `id` int(225) NOT NULL,
  `username` varchar(100) NOT NULL,
  `id_status` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_status`
--

INSERT INTO `user_status` (`id`, `username`, `id_status`) VALUES
(13, 'adenovendra19', 1),
(14, 'pakeko', 1),
(15, 'emon', 1),
(16, 'pakemon', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `friend_request`
--
ALTER TABLE `friend_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pemberi_info`
--
ALTER TABLE `pemberi_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_status`
--
ALTER TABLE `user_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `friend_request`
--
ALTER TABLE `friend_request`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pemberi_info`
--
ALTER TABLE `pemberi_info`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user_status`
--
ALTER TABLE `user_status`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
